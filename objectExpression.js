"use strict";


function createExpression(expression, evaluate, toString, prefix, postfix, diff) {
    expression.prototype.evaluate = evaluate;
    expression.prototype.toString = toString;
    expression.prototype.prefix = prefix;
    expression.prototype.postfix = postfix;
    expression.prototype.diff = diff;
}

function Operation(...elements) {
    this.elements = elements;
}

createExpression(Operation, function(...values)
    {return this.count(...(this.elements.map(el => el.evaluate(...values))));},
    function() {return this.elements.map(el => el.toString()).join(" ") + " " + this.symbol;},
    function () {return "(" + this.symbol + " " + this.elements.map(el => el.prefix()).join(" ") + ")";},
    function () {return "(" + this.elements.map(el => el.postfix()).join(" ") + " " + this.symbol + ")";});

Operation.operations = {
    "+": [Add, 2],
    "-": [Subtract, 2],
    "*": [Multiply, 2],
    "/": [Divide, 2],
    "negate": [Negate, 1],
    "gauss": [Gauss, 4],
    "exp": [Exp, 1],
    "sumexp": [Sumexp, 0],
    "softmax": [Softmax, 0],
};
Operation.createOperation = function(operation, count, symbol, diff) {
    operation.prototype = Object.create(Operation.prototype);
    operation.prototype.count = count;
    operation.prototype.symbol = symbol;
    operation.prototype.diff = diff;
}

function Add(x, y) {
    Operation.call(this, x, y);
}
Operation.createOperation(Add, (x, y) => x + y, "+",
    function (v) {return new Add(this.elements[0].diff(v), this.elements[1].diff(v));});

function Subtract(x, y) {
    Operation.call(this, x, y);
}
Operation.createOperation(Subtract, (x, y) => x - y, "-",
    function(v) {return new Subtract(this.elements[0].diff(v), this.elements[1].diff(v));});


function Multiply(x, y) {
    Operation.call(this, x, y);
}
Operation.createOperation(Multiply, (x, y) => x * y, "*",
    function(v) { return new Add(new Multiply(this.elements[0], this.elements[1].diff(v)),
        new Multiply(this.elements[0].diff(v), this.elements[1]));});

function Divide(x, y) {
    Operation.call(this, x, y);
}
Operation.createOperation(Divide, (x, y) => x / y, "/",
    function(v) {return new Divide(new Subtract(new Multiply(this.elements[0].diff(v), this.elements[1]),
        new Multiply(this.elements[0], this.elements[1].diff(v))), new Multiply(this.elements[1], this.elements[1]));});


function Negate(x) {
    Operation.call(this, x);
}
Operation.createOperation(Negate, (x) => -x, "negate",
    function(v) {return new Negate(this.elements[0].diff(v));});


function Exp(x) {
    Operation.call(this, x);
}
Operation.createOperation(Exp, (x) => Math.pow(Math.E, x), "exp", function (v)
{return new Multiply(new Exp(this.elements[0]), this.elements[0].diff(v))});



function Gauss(a, b, c, x) {
    Operation.call(this, a, b, c, x);
}
Operation.createOperation(Gauss, (a, b, c, x) => a * Math.pow(Math.E, -(x - b)*(x - b)/(2 * c * c)),
    "gauss", function(v) {let a, b, c, x;[a, b, c, x] = this.elements;
        let power = new Divide(new Negate(new Multiply(new Subtract(x, b), new Subtract(x, b))),
            new Multiply(new Multiply(Const.TWO, c), c));
        return new Add(new Multiply(a.diff(v), new Divide(this, a)), new Multiply(new Multiply(a,
            new Divide(this, a)), power.diff(v)));});


function Sumexp(...elements) {
    Operation.call(this, ...elements);
}
Operation.createOperation(Sumexp, (...values) =>
        values.map(el => Math.pow(Math.E, el)).reduce((a, b) => a + b, 0),
    "sumexp", function(v) {return this.elements.map(el =>
        new Multiply(new Exp(el), el.diff(v))).reduce((a, b) => new Add(a, b), Const.ZERO);});


function Softmax(...elements) {
    Operation.call(this, ...elements);
}
Operation.createOperation(Softmax, (...values) => Math.pow(Math.E, values[0]) / new Sumexp().count(...values),
    "softmax", function (v) {return new Divide(new Subtract(new Multiply(new Exp(this.elements[0]).diff(v),
        new Sumexp(...this.elements)), new Multiply(new Exp(this.elements[0]), new Sumexp(...this.elements).diff(v))),
        new Multiply(new Sumexp(...this.elements), new Sumexp(...this.elements)))});


function Const(value) {
    this.value = value;
}
createExpression(Const, function() { return this.value},
    function() {return this.value.toString();},
    function() {return this.value.toString();}, function() {return this.value.toString();},
    () => Const.ZERO);

Const.ONE = new Const(1);
Const.ZERO = new Const(0);
Const.TWO = new Const(2);


function Variable(v) {
    this.var = v;
    this.argument = Variable.variables[v];
}
Variable.variables = {
    "x": 0,
    "y": 1,
    "z": 2
};
createExpression(Variable, function(...values) { return values[this.argument]},
    function() {return this.var;}, function() {
        return this.var; },function() {return this.var; },
    function(v) {return v === this.var ? Const.ONE : Const.ZERO;});


const parse = function (expr) {
    let vars = expr.trim().split(/\s+/);
    let stack = [];
    vars.map(el => stack.push(el in Operation.operations ?
        new Operation.operations[el][0](...stack.splice(-Operation.operations[el][1])) :
        el in Variable.variables ? new Variable(el) : new Const(+el)));
    return stack.pop();
}

const createCustomError = function(parentError, customError) {
    customError.prototype = Object.create(parentError.prototype);
    customError.prototype.constructor = customError;
}

const ParsingError = function(expectedValue, index, parsedValue) {
    this.message = `Expected ${expectedValue} at ${index}, found '${parsedValue}' instead`;
    Error.call(this, this.message);
}
createCustomError(Error, ParsingError);

const WrongOperationError = function (index, parsedValue) {
    ParsingError.call(this, "known operation", index, parsedValue);
}
createCustomError(ParsingError, WrongOperationError);

const EndOfExpressionError = function (index, parsedValue) {
    ParsingError.call(this, "end of expression", index, parsedValue);
}
createCustomError(ParsingError, EndOfExpressionError);

const NumberOfArgumentsError = function (requiredN, index, foundN) {
    ParsingError.call(this, requiredN + " arguments", index, foundN);
}
createCustomError(ParsingError, EndOfExpressionError);


let parser = (function (expr) {
    let index = 0;
    let token_index = 0;

    const skipWhitespace = function () {
        while (index < expr.length && /\s/.test(expr[index])) {
            index++;
        }
    }

    const parseToken = function () {
        skipWhitespace();
        let token = "";
        token_index = index;
        while (index < expr.length && !(/\s/.test(expr[index])) && expr[index] !== ")" && expr[index] !== "(") {
            token = token.concat(expr[index]);
            index++;
        }
        skipWhitespace();
        return token;
    }

    const parse = function (mode) {
        let result = parseExpression(modes[mode]);
        skipWhitespace();
        if (index < expr.length && !/\s/.test(expr[index])) {
            throw new EndOfExpressionError(index, expr[index]);
        }
        return result;
    }

    const parsePrefix = function () {
        let token = parseToken();
        let op = token in Operation.operations ? Operation.operations[token] : null;
        if (op === null) {
            throw new WrongOperationError(token_index, token);
        }
        let stack = [];
        while (expr[index] !== ")") {
            let token = parseExpression(parsePrefix);
            stack.push(token);
            skipWhitespace();
        }
        if (op[1] !== 0 && stack.length !== op[1]) {
            throw new NumberOfArgumentsError(op[1], token_index, stack.length);
        }
        op = new op[0](...stack);
        return op;
    }

    const parsePostfix = function () {
        let stack = [];
        let token = parseExpression(parsePostfix);
        while (!(token in Operation.operations)) {
            stack.push(token);
            token = parseExpression(parsePostfix);
        }
        if (Operation.operations[token][1] !== 0 && Operation.operations[token][1] !== stack.length) {
            throw new NumberOfArgumentsError(Operation.operations[token][1], token_index, stack.length);
        }
        return new Operation.operations[token][0](...stack.splice(-Operation.operations[token][1]));
    }

    const parseExpression = function (impl) {
        skipWhitespace();
        if (expr[index] !== "(") {
            let token = parseToken();
            token = token in Variable.variables ? new Variable(token) : !Number.isNaN(+token) && token !== "" ?
                new Const(+token) : token in Operation.operations && impl === parsePostfix ?
                    token : null;
            if (token === null) {
                throw new ParsingError("'('", token_index, expr[token_index]);
            }
            return token;
        }
        index++;
        skipWhitespace();
        let op = impl();
        skipWhitespace();
        if (expr[index++] !== ")") {
            throw new ParsingError("')'", index - 1, expr[index - 1]);
        }
        return op;
    }
    const modes = {
        "prefix": parsePrefix,
        "postfix": parsePostfix,
    };
    return {parse};
});

let parsePrefix = function(expr) {
    return parser(expr).parse("prefix");
}

let parsePostfix = function(expr) {
    return parser(expr).parse("postfix");
}
