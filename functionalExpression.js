"use strict";


const abstractOperation = op => (...elements) => (x, y, z) => op(...(elements.map(el => el(x, y, z))));

const cnst = value => () => value;
const add = abstractOperation((a, b) => a + b);
const subtract = abstractOperation((a, b) => a - b);
const multiply = abstractOperation((a, b) => a * b);
const divide = abstractOperation((a, b) => a / b);
const negate = abstractOperation(a => -a);

const variables = {
    "x": 0,
    "y": 1,
    "z": 2
};
const variable = function(str) {
    let i = variables[str];
    return (...elements) => elements[i];
}

const avg3 = abstractOperation((a, b, c) => (a + b + c) / 3);
const med5 = abstractOperation((...elements) =>
    elements.sort((a, b)  => a - b)[~~(elements.length / 2)]);
const pi = cnst(Math.PI);
const e = cnst(Math.E);

const operations = {
    "+": [add, 2],
    "-": [subtract, 2],
    "*": [multiply, 2],
    "/": [divide, 2],
    "negate": [negate, 1],
    "avg3": [avg3, 3],
    "med5": [med5, 5],
};
const CONSTANTS = {
    "e": e,
    "pi": pi
};

const parse = function (expr) {
    let vars = expr.trim().split(/\s+/);
    let stack = [];
    vars.map(el => stack.push(el in operations ? operations[el][0](...stack.splice(-operations[el][1])) :
        el in variables ? variable(el) : el in CONSTANTS ? CONSTANTS[el] : cnst(+el)));
    return stack.pop();
}